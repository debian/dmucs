#!/bin/sh 
### BEGIN INIT INFO
# Provides:          dmucs
# Should-Start:      loadavg
# Required-Start:    $remote_fs
# Required-Stop:     $remote_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: <Enter a short description of the sortware>
# Description:       <Enter a long description of the software>
#                    <...>
#                    <...>
### END INIT INFO

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

NAME=dmucs

DAEMON=/usr/sbin/loadavg
NAME2=loadavg
DAEMON_WRAPPER=$DAEMON
DESC="dmucs client daemon"
PIDFILE=/var/run/dmucs_loadavg.pid

SERVER_DAEMON=/usr/sbin/dmucs
SERVER_DAEMON_WRAPPER=$SERVER_DAEMON
SERVER_NAME2=dmucs
SERVER_DESC="dmucs distcc coordinator"
SERVER_PIDFILE=/var/run/dmucs.pid

LOGDIR=/var/log/dmucs

test -x $DAEMON || exit 0
test -x $DAEMON_WRAPPER || exit 0

test -x $SERVER_DAEMON || exit 0
test -x $SERVER_DAEMON_WRAPPER || exit 0

. /lib/lsb/init-functions


DODTIME=10              # Time to wait for the server to die, in seconds
                        # If this value is set too low you might not
                        # let some servers to die gracefully and
                        # 'restart' will not work

LOGFILE=$LOGDIR/$NAME.log  # Server logfile
DAEMONUSER=nobody

# Include defaults if available
if [ -f /etc/default/$NAME ] ; then
    . /etc/default/$NAME
fi

if [ -z "$USE_SERVER" -a ! "$SERVER" = "yes" ]; then
     log_warning_msg "please edit /etc/default/$NAME to enable dmucs"
     exit 0
fi

DAEMON_OPTS="-s $USE_SERVER"
SERVER_DAEMON_OPTS=""
set -e

running_pid() {
# Check if a given process pid's cmdline matches a given name
    pid=$1
    name=$2
    [ -z "$pid" ] && return 1 
    [ ! -d /proc/$pid ] &&  return 1
    cmd=`cat /proc/$pid/cmdline | tr "\000" "\n"|head -n 1 |cut -d : -f 1`
    # Is this the expected server
    [ "$cmd" != "$name" ] &&  return 1
    return 0
}

running() {
    # Check if the process is running looking at /proc
    # (works for all users)

    # No pidfile, probably no daemon present
    [ ! -f "$PIDFILE" ] && return 1
    pid=`cat $PIDFILE`
    running_pid $pid $DAEMON_WRAPPER || return 1
    return 0
}

server_running() {
    # Check if the process is running looking at /proc
    # (works for all users)

    # No pidfile, probably no daemon present
    [ ! -f "$SERVER_PIDFILE" ] && return 1
    pid=`cat $SERVER_PIDFILE`
    running_pid $pid $SERVER_DAEMON_WRAPPER || return 1
    return 0
}

start_server() {
        start-stop-daemon --background --make-pidfile --start --pidfile $SERVER_PIDFILE --exec $SERVER_DAEMON -- $SERVER_DAEMON_OPTS
        errcode=$?
        return $errcode
}

start_loadavg() {
        start-stop-daemon --background --make-pidfile --start --pidfile $PIDFILE --exec $DAEMON -- $DAEMON_OPTS
        errcode=$?
        return $errcode
}

stop_loadavg() {
        start-stop-daemon --make-pidfile --stop --pidfile $PIDFILE --exec $DAEMON
        errcode=$?
        return $errcode
}

stop_server() {
        start-stop-daemon --make-pidfile --stop --pidfile $SERVER_PIDFILE --exec $SERVER_DAEMON
        errcode=$?
        return $errcode
}

force_stop() {
# Force the process to die killing it manually
	[ ! -e "$PIDFILE" ] && return
	if running ; then
		kill -15 $pid
	# Is it really dead?
		sleep "$DIETIME"s
		if running ; then
			kill -9 $pid
			sleep "$DIETIME"s
			if running ; then
				echo "Cannot kill $NAME (pid=$pid)!"
				exit 1
			fi
		fi
	fi
	rm -f $PIDFILE
}


case "$1" in
  start)
        if [ "$SERVER" = "yes" ]; then
            log_daemon_msg "Starting $SERVER_DESC " "$SERVER_NAME2"
            # Check if it's running first
            if server_running ;  then
                log_progress_msg "apparently already running"
                log_end_msg 0
                exit 0
            fi
            if start_server && server_running ;  then
                # It's ok, the server started and is running
                log_end_msg 0
            else
                # Either we could not start it or it is not running
                # after we did
                # NOTE: Some servers might die some time after they start,
                # this code does not try to detect this and might give
                # a false positive (use 'status' for that)
                log_end_msg 1
            fi
        fi

        if [ "$USE_SERVER" ]; then
            log_daemon_msg "Starting $DESC " "$NAME2"
            # Check if it's running first
            if running ;  then
                log_progress_msg "apparently already running"
                log_end_msg 0
                exit 0
            fi
            if start_loadavg && running ;  then
                # It's ok, the server started and is running
                log_end_msg 0
            else
                # Either we could not start it or it is not running
                # after we did
                # NOTE: Some servers might die some time after they start,
                # this code does not try to detect this and might give
                # a false positive (use 'status' for that)
                log_end_msg 1
            fi
        fi
        ;;
  stop)
        if [ "$SERVER" = "yes" ]; then
            log_daemon_msg "Stopping $SERVER_DESC" "$SERVER_NAME2"
            if server_running ; then
                # Only stop the server if we see it running
                stop_server
                log_end_msg $?
            else
                # If it's not running don't do anything
                log_progress_msg "apparently not running"
                log_end_msg 0
                exit 0
            fi
        fi
        if [ "$USE_SERVER" ]; then
            log_daemon_msg "Stopping $DESC" "$NAME2"
            if running ; then
                # Only stop the server if we see it running
                stop_loadavg
                log_end_msg $?
            else
                # If it's not running don't do anything
                log_progress_msg "apparently not running"
                log_end_msg 0
                exit 0
            fi
        fi
        ;;
  force-stop)
        # First try to stop gracefully the program
        $0 stop
        if running; then
            # If it's still running try to kill it more forcefully
            log_daemon_msg "Stopping (force) $DESC" "$NAME2"
            force_stop
            log_end_msg $?
        fi
        ;;
  restart|force-reload)
        if [ "$SERVER" = "yes" ]; then
            log_daemon_msg "Restarting $SERVER_DESC" "$SERVER_NAME2"
            stop_server
            # Wait some sensible amount, some server need this
            [ -n "$DIETIME" ] && sleep $DIETIME
            start_server
            running
            log_end_msg $?
        fi
        if [ "$USE_SERVER" ]; then
            log_daemon_msg "Restarting $DESC" "$NAME2"
            stop_server
            # Wait some sensible amount, some server need this
            [ -n "$DIETIME" ] && sleep $DIETIME
            start_server
            running
            log_end_msg $?
        fi
        ;;
  status)
        log_daemon_msg "Checking status of $DESC" "$NAME2"
        if running ;  then
            log_progress_msg "running"
            log_end_msg 0
        else
            log_progress_msg "apparently not running"
            log_end_msg 1
            exit 1
        fi
        ;;
  # Use this if the daemon cannot reload
  reload)
        log_warning_msg "Reloading $NAME daemon: not implemented, as the daemon"
        log_warning_msg "cannot re-read the config file (use restart)."
        ;;
  *)
	N=/etc/init.d/$NAME
	echo "Usage: $N {start|stop|force-stop|restart|force-reload|status}" >&2
	exit 1
	;;
esac

exit 0
