Source: dmucs
Section: devel
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends: debhelper-compat (= 13)
Rules-Requires-Root: no
Standards-Version: 4.7.0
Homepage: http://dmucs.sourceforge.net/
Vcs-Git: https://salsa.debian.org/debian/dmucs.git
Vcs-Browser: https://salsa.debian.org/debian/dmucs

Package: dmucs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Recommends: distcc
Description: distributed compilation system for use with distcc
 DMUCS is a system that allows a group of users to share a compilation farm.
 Each compilation request from each user will be sent to the fastest available
 machine, every time.  The system has these fine qualities:
  * Works with distcc, which need not be altered in any way.
  * Supports multiple operating systems in the compilation farm.
  * Uses all processors of a multi-processor compilation host.
  * Makes best use of compilation hosts with widely differing CPU speeds.
  * Takes into account the load on a host caused by non-compilation tasks.
  * Supports the dynamic addition and removal of hosts to the compilation farm.
